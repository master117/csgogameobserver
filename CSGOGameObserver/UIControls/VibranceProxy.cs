﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace CSGOGameObserver.UIControls
{
    [StructLayout(LayoutKind.Sequential)]
    public struct VIBRANCE_INFO
    {
        public bool isInitialized;
        public int activeOutput;
        public int defaultHandle;
        public int userVibranceSettingDefault;
        public int userVibranceSettingActive;
        public String szGpuName;
        public bool shouldRun;
        public bool keepActive;
        public int sleepInterval;
        public List<int> displayHandles;
    }
    
    public class VibranceProxy
    {

        /*
        #region DLL Imports

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?initializeLibrary@vibrance@vibranceDLL@@QAE_NXZ",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        static extern bool initializeLibrary();

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?unloadLibrary@vibrance@vibranceDLL@@QAE_NXZ",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        static extern bool unloadLibrary();

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?getActiveOutputs@vibrance@vibranceDLL@@QAEHQAPAH0@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        static extern int getActiveOutputs([In, Out] int[] gpuHandles, [In, Out] int[] outputIds);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?enumeratePhsyicalGPUs@vibrance@vibranceDLL@@QAEXQAPAH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        static extern void enumeratePhsyicalGPUs([In, Out] int[] gpuHandles);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?getGpuName@vibrance@vibranceDLL@@QAE_NQAPAHPAD@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Ansi)]
        static extern bool getGpuName([In, Out] int[] gpuHandles, StringBuilder szName);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?enumerateNvidiaDisplayHandle@vibrance@vibranceDLL@@QAEHH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        static extern int enumerateNvidiaDisplayHandle(int index);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?setDVCLevel@vibrance@vibranceDLL@@QAE_NHH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern bool setDVCLevel([In] int defaultHandle, [In] int level);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?isCsgoActive@vibrance@vibranceDLL@@QAE_NPAPAUHWND__@@@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern bool isCsgoActive(ref IntPtr hwnd);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?isCsgoStarted@vibrance@vibranceDLL@@QAE_NPAPAUHWND__@@@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Ansi)]
        public static extern bool isCsgoStarted(ref IntPtr hwnd);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?getAssociatedNvidiaDisplayHandle@vibrance@vibranceDLL@@QAEHPBDH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Ansi)]
        static extern int getAssociatedNvidiaDisplayHandle(string deviceName, [In] int length);

        #endregion
        */

        #region DllImports
        [StructLayout(LayoutKind.Sequential)]
        public struct NvDisplayDvcInfo
        {
            public uint version;
            public int currentLevel;
            public int minLevel;
            public int maxLevel;
            public NvDisplayDvcInfo(uint version, int currentLevel, int minLevel, int maxLevel)
            {
                this.version = version;
                this.currentLevel = currentLevel;
                this.minLevel = minLevel;
                this.maxLevel = maxLevel;
            }
        }

        enum NvApiStatus
        {
            NvapiOk = 0, NvapiError = -1, NvapiLibraryNotFound = -2, NvapiNoImplementation = -3,
            NvapiApiNotInitialized = -4, NvapiInvalidArgument = -5, NvapiNvidiaDeviceNotFound = -6, NvapiEndEnumeration = -7,
            NvapiInvalidHandle = -8, NvapiIncompatibleStructVersion = -9, NvapiHandleInvalidated = -10, NvapiOpenglContextNotCurrent = -11,
            NvapiInvalidPointer = -14, NvapiNoGlExpert = -12, NvapiInstrumentationDisabled = -13, NvapiNoGlNsight = -15,
            NvapiExpectedLogicalGpuHandle = -100, NvapiExpectedPhysicalGpuHandle = -101, NvapiExpectedDisplayHandle = -102, NvapiInvalidCombination = -103,
            NvapiNotSupported = -104, NvapiPortidNotFound = -105, NvapiExpectedUnattachedDisplayHandle = -106, NvapiInvalidPerfLevel = -107,
            NvapiDeviceBusy = -108, NvapiNvPersistFileNotFound = -109, NvapiPersistDataNotFound = -110, NvapiExpectedTvDisplay = -111,
            NvapiExpectedTvDisplayOnDconnector = -112, NvapiNoActiveSliTopology = -113, NvapiSliRenderingModeNotallowed = -114, NvapiExpectedDigitalFlatPanel = -115,
            NvapiArgumentExceedMaxSize = -116, NvapiDeviceSwitchingNotAllowed = -117, NvapiTestingClocksNotSupported = -118, NvapiUnknownUnderscanConfig = -119,
            NvapiTimeoutReconfiguringGpuTopo = -120, NvapiDataNotFound = -121, NvapiExpectedAnalogDisplay = -122, NvapiNoVidlink = -123,
            NvapiRequiresReboot = -124, NvapiInvalidHybridMode = -125, NvapiMixedTargetTypes = -126, NvapiSyswow64NotSupported = -127,
            NvapiImplicitSetGpuTopologyChangeNotAllowed = -128, NvapiRequestUserToCloseNonMigratableApps = -129, NvapiOutOfMemory = -130, NvapiWasStillDrawing = -131,
            NvapiFileNotFound = -132, NvapiTooManyUniqueStateObjects = -133, NvapiInvalidCall = -134, NvapiD3D101LibraryNotFound = -135,
            NvapiFunctionNotFound = -136, NvapiInvalidUserPrivilege = -137, NvapiExpectedNonPrimaryDisplayHandle = -138, NvapiExpectedComputeGpuHandle = -139,
            NvapiStereoNotInitialized = -140, NvapiStereoRegistryAccessFailed = -141, NvapiStereoRegistryProfileTypeNotSupported = -142, NvapiStereoRegistryValueNotSupported = -143,
            NvapiStereoNotEnabled = -144, NvapiStereoNotTurnedOn = -145, NvapiStereoInvalidDeviceInterface = -146, NvapiStereoParameterOutOfRange = -147,
            NvapiStereoFrustumAdjustModeNotSupported = -148, NvapiTopoNotPossible = -149, NvapiModeChangeFailed = -150, NvapiD3D11LibraryNotFound = -151,
            NvapiInvalidAddress = -152, NvapiStringTooSmall = -153, NvapiMatchingDeviceNotFound = -154, NvapiDriverRunning = -155,
            NvapiDriverNotrunning = -156, NvapiErrorDriverReloadRequired = -157, NvapiSetNotAllowed = -158, NvapiAdvancedDisplayTopologyRequired = -159,
            NvapiSettingNotFound = -160, NvapiSettingSizeTooLarge = -161, NvapiTooManySettingsInProfile = -162, NvapiProfileNotFound = -163,
            NvapiProfileNameInUse = -164, NvapiProfileNameEmpty = -165, NvapiExecutableNotFound = -166, NvapiExecutableAlreadyInUse = -167,
            NvapiDatatypeMismatch = -168, NvapiProfileRemoved = -169, NvapiUnregisteredResource = -170, NvapiIdOutOfRange = -171,
            NvapiDisplayconfigValidationFailed = -172, NvapiDpmstChanged = -173, NvapiInsufficientBuffer = -174, NvapiAccessDenied = -175,
            NvapiMosaicNotActive = -176, NvapiShareResourceRelocated = -177, NvapiRequestUserToDisableDwm = -178, NvapiD3DDeviceLost = -179,
            NvapiInvalidConfiguration = -180, NvapiStereoHandshakeNotDone = -181, NvapiExecutablePathIsAmbiguous = -182, NvapiDefaultStereoProfileIsNotDefined = -183,
            NvapiDefaultStereoProfileDoesNotExist = -184, NvapiClusterAlreadyExists = -185, NvapiDpmstDisplayIdExpected = -186, NvapiInvalidDisplayId = -187,
            NvapiStreamIsOutOfSync = -188, NvapiIncompatibleAudioDriver = -189, NvapiValueAlreadySet = -190, NvapiTimeout = -191,
            NvapiGpuWorkstationFeatureIncomplete = -192, NvapiStereoInitActivationNotDone = -193, NvapiSyncNotActive = -194, NvapiSyncMasterNotFound = -195,
            NvapiInvalidSyncTopology = -196
        };

        public enum NvSystemType
        {
            NvSystemTypeUnknown = 0,
            NvSystemTypeLaptop = 1,
            NvSystemTypeDesktop = 2
        };

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?initializeLibrary@vibrance@vibranceDLL@@QAE_NXZ",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern bool initializeLibrary();

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?unloadLibrary@vibrance@vibranceDLL@@QAE_NXZ",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern bool unloadLibrary();


        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?getActiveOutputs@vibrance@vibranceDLL@@QAEHQAPAH0@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern int getActiveOutputs([In, Out] int[] gpuHandles, [In, Out] int[] outputIds);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?enumeratePhsyicalGPUs@vibrance@vibranceDLL@@QAEXQAPAH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern void enumeratePhsyicalGPUs([In, Out] int[] gpuHandles);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?getGpuName@vibrance@vibranceDLL@@QAE_NQAPAHPAD@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Ansi)]
        public static extern bool getGpuName([In, Out] int[] gpuHandles, StringBuilder szName);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?getDVCInfo@vibrance@vibranceDLL@@QAE_NPAUNV_DISPLAY_DVC_INFO@12@H@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Ansi)]
        public static extern bool getDVCInfo(ref NvDisplayDvcInfo info, int defaultHandle);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?enumerateNvidiaDisplayHandle@vibrance@vibranceDLL@@QAEHH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern int enumerateNvidiaDisplayHandle(int index);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?setDVCLevel@vibrance@vibranceDLL@@QAE_NHH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern bool setDVCLevel([In] int defaultHandle, [In] int level);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?isWindowActive@vibrance@vibranceDLL@@QAE_NPAPAUHWND__@@@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern bool isWindowActive(ref IntPtr hwnd);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?isCsgoStarted@vibrance@vibranceDLL@@QAE_NPAPAUHWND__@@@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Ansi)]
        public static extern bool isCsgoStarted(ref IntPtr hwnd);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?equalsDVCLevel@vibrance@vibranceDLL@@QAE_NHH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern bool equalsDVCLevel([In] int defaultHandle, [In] int level);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?getGpuSystemType@vibrance@vibranceDLL@@QAEHPAH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Auto)]
        public static extern NvSystemType getGpuSystemType(int gpuHandle);

        [DllImport("user32.dll", CharSet = CharSet.Ansi)]
        public static extern int GetWindowTextLength([In] IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Ansi)]
        public static extern int GetWindowTextA([In] IntPtr hWnd, [In, Out] StringBuilder lpString, [In] int nMaxCount);

        [DllImport(
            "vibranceDLL.dll",
            EntryPoint = "?getAssociatedNvidiaDisplayHandle@vibrance@vibranceDLL@@QAEHPBDH@Z",
            CallingConvention = CallingConvention.StdCall,
            CharSet = CharSet.Ansi)]
        public static extern int getAssociatedNvidiaDisplayHandle(string deviceName, [In] int length);
        #endregion

        public const int NVAPI_MAX_PHYSICAL_GPUS = 64;

        public const string NVAPI_ERROR_INIT_FAILED = "VibranceProxy failed to initialize! Read readme.txt for fix!";

        public VIBRANCE_INFO VibranceInfo;

        public VibranceProxy()
        {
            try
            {
                VibranceInfo = new VIBRANCE_INFO();
                bool ret = initializeLibrary();

                int[] gpuHandles = new int[NVAPI_MAX_PHYSICAL_GPUS];
                int[] outputIds = new int[NVAPI_MAX_PHYSICAL_GPUS];
                enumeratePhsyicalGPUs(gpuHandles);

                EnumerateDisplayHandles();
               
                VibranceInfo.activeOutput = getActiveOutputs(gpuHandles, outputIds);
                StringBuilder buffer = new StringBuilder(64);
                char[] sz = new char[64];
                getGpuName(gpuHandles, buffer);
                VibranceInfo.szGpuName = buffer.ToString();
                VibranceInfo.defaultHandle = enumerateNvidiaDisplayHandle(0);
                VibranceInfo.isInitialized = true;
            }
            catch (Exception)
            {
                MessageBox.Show(VibranceProxy.NVAPI_ERROR_INIT_FAILED);
            }

        }

        public int GetCsgoDisplayHandle()
        {
            IntPtr hwnd = IntPtr.Zero;
            if (isCsgoStarted(ref hwnd) && hwnd != IntPtr.Zero)
            {
                var primaryScreen = System.Windows.Forms.Screen.FromHandle(hwnd);

                string deviceName = primaryScreen.DeviceName;
                GCHandle handle = GCHandle.Alloc(deviceName, GCHandleType.Pinned);
                int id = getAssociatedNvidiaDisplayHandle(deviceName, deviceName.Length);
                handle.Free();

                return id;
            }

            return -1;
        }

        public bool UnloadLibraryEx()
        {
            return unloadLibrary();
        }

        private void EnumerateDisplayHandles()
        {
            int displayHandle = 0;
            for (int i = 0; displayHandle != -1; i++)
            {
                if (VibranceInfo.displayHandles == null)
                    VibranceInfo.displayHandles = new List<int>();

                displayHandle = enumerateNvidiaDisplayHandle(i);
                if (displayHandle != -1)
                    VibranceInfo.displayHandles.Add(displayHandle);
            }
        }
    }
}
